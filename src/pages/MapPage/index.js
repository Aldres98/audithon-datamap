import React, {useEffect, useState} from 'react';
import { Grid, Button } from '@material-ui/core';
import NavigationDrawer from 'components/NavigationDrawer'
import { makeStyles } from '@material-ui/core/styles';
import DeckMap from 'components/DeckMap';
import Timeline from 'components/Timeline';
import CloseStorytellingBtn from 'components/CloseStorytellingBtn';
import Storytelling  from 'components/Storytelling';
import {useDispatch, useSelector} from "react-redux";
import {hiddenStory, openMoneyModal} from "redux/actions";
import MoneyModal from "components/MoneyModal";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 'none!important',
    width: '100%',
    height: '100vh',
    padding: 0,
  },
  timeline: {
    position: 'fixed',
    bottom: '60px',
    left: 0,
    width: '95%',
    margin: '0 2.5%',
    fontSize: '12px',
    transition: '.5s',
  }
}));

function MapPage() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const isStoryShown = useSelector((state) => state.scrollama.isShown);
  const isStoryHidden = useSelector((state) => state.scrollama.isHidden);
  const currentStory = useSelector((state) => state.scrollama.currentStory.story);
  const isMoneyModalOpen = useSelector((state) => state.app.isMoneyModalOpen);
  const [isModalOpen, setModalOpen] = useState(false);

  useEffect(() => {
    if(isMoneyModalOpen !== isModalOpen){
      setModalOpen(isMoneyModalOpen);
    }
  }, [isMoneyModalOpen]);

  useEffect(() => {
    if(isMoneyModalOpen !== isModalOpen){
      dispatch(openMoneyModal(isModalOpen));
    }
  }, [isModalOpen]);

  return (
    <Grid container className={classes.root}>
      {isStoryShown && !isStoryHidden &&
        <CloseStorytellingBtn/>
      }
      <Storytelling/>
      <DeckMap />
      <div style={{ position: 'fixed', top: '15px', left: '15px', color: "#fff" }}>
        {isStoryHidden &&
          <h2>{currentStory.title}</h2>
        }
        {!isStoryShown &&
        <>
          <h2>Выберите район на карте</h2>
          <p>Приблизьте карту для просмотра муниципалитетов</p>
        </>
        }
      </div>
      <div style={{ position: 'fixed', top: '15px', right: '15px' }}>
        {isStoryHidden &&
          <Button variant={"contained"} color="primary" onClick={() => dispatch(hiddenStory())}>Вернуться к статистике</Button>
        }
      </div>
      <Grid className={classes.timeline} style={{ bottom: (isStoryShown && !isStoryHidden ? '-60px' : '70px') }}>
        <Timeline />
      </Grid>
      <MoneyModal isOpen={isModalOpen} handler={setModalOpen} />
    </Grid>
  );
}

export default MapPage;
