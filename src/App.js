import React from 'react';
import { Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Route, Switch } from 'react-router-dom';
import MapPage from 'pages/MapPage';

const useStyles = makeStyles(() => ({
  root: {
    maxWidth: 'none!important',
    width: '100%',
    height: '100vh',
    padding: 0,
  },
}));

function App() {
  const classes = useStyles();
  return (
    <Container className={classes.root} fixed>
      <Switch>
        <Route exact path={"/"} component={MapPage} />
      </Switch>
    </Container>
  );
}

export default App;
