export const MAPBOX = {
  API_TOKEN:
    'pk.eyJ1IjoiZG9uaW5wciIsImEiOiJjaXVxM3E4OTkwMDAwMm9wczBnYjR4bnJoIn0.7drt92qBRl7KJ6dLg0mrww',
  VIEWPORT: {
    latitude: 72.36,
    longitude: 111.16,
    zoom: 2,
    mapStyle: 'mapbox://styles/doninpr/ckmrol83r29xj17q3m9gsjf8d/draft',
  },
};
