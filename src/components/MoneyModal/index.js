import React, { useState, useEffect } from "react";
import { Grid, TextField, Typography, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "components/Modal";
import { useSelector, useDispatch } from "react-redux";
import { fetchCosts, fetchInvestments } from "redux/actions";
import { Line } from "react-chartjs-2";
import { each } from "jquery";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(1),
    textAlign: "center",
  },
  input: {
    width: "90%",
    margin: theme.spacing(1),
  },
  mod: {
    width: "800px",
    height: "650px",
  },
}));

function ModalComponent(params) {
  const { isOpen, handler } = params;
  const classes = useStyles();

  const dynamicColors = function () {
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return "rgb(" + r + "," + g + "," + b + ")";
  };

  const costs = useSelector((state) => state.scrollama.costs);
  const investments = useSelector((state) => state.scrollama.investments);
  const [investmentsData, setInvestmentsData] = useState({
    labels: [2017, 2018, 2019],
    datasets: [
      {
        fill: false,
        backgroundColor: "255,255,255",
        borderColor: dynamicColors,
        label:
          "Инвестициии в охрану окружающей среды и рациональное использование природных ресурсов",
        data: ["1760414", "", "34713930"],
      },

      {
        fill: false,
        backgroundColor: "255,255,255",
        borderColor: dynamicColors,
        label: "Охрана и рациональное использование водных ресурсов",
        data: ["2966749", "2046254", "4990251"],
      },

      {
        fill: false,
        backgroundColor: "255,255,255",
        borderColor: dynamicColors,
        label: "Инвестициии в охрану атмосферного воздуха",
        data: ["18873250", "21392574", "23042457"],
      },

      {
        fill: false,
        backgroundColor: "255,255,255",
        borderColor: dynamicColors,
        label: "Инвестициии в охрану и рациональное использование земель",
        data: ["766632", "395387", "2649890"],
      },

      {
        fill: false,
        backgroundColor: "255,255,255",
        borderColor: dynamicColors,
        label:
          "Инвестициии в охрану окружающей среды от вредного воздействия отходов производства и потребления",
        data: ["1550649", "2193171", "3350962"],
      },

      {
        fill: false,
        backgroundColor: "255,255,255",
        borderColor: dynamicColors,
        label: "Прочие направления охраны",
        data: ["1550649", "2193171", "3350962"],
      },
    ],
  });

  const [chartsData, setChartsData] = useState({
    labels: [2017, 2018, 2019],
    datasets: [
      {
        fill: false,
        backgroundColor: "255,255,255",
        borderColor: dynamicColors,
        label: "Затраты на обращение с отходами",
        data: ["14280562", "15673951", "19124577.99"],
      },

      {
        fill: false,
        backgroundColor: "255,255,255",
        borderColor: dynamicColors,

        label: "Затраты на сбор и очистку сточных вод",
        data: ["10087688", "9561319", "11243827.99"],
      },

      {
        fill: false,
        backgroundColor: "255,255,255",
        borderColor: dynamicColors,

        label:
          "Затраты на охрану атмосферного воздуха и предотвращение изменения климата",
        data: ["4540887", "4084382", "4333564.99"],
      },

      {
        fill: false,
        backgroundColor: "255,255,255",
        borderColor: dynamicColors,

        label:
          "Затраты на обеспечение радиационной безопасности окружающей среды",
        data: ["723938", "1306371", "1284674"],
      },

      {
        fill: false,
        backgroundColor: "255,255,255",
        borderColor: dynamicColors,

        label:
          "Затраты на защиту реабилитации земель, поверхностных и подземных вод",
        data: ["1506047", "131451", "1403464"],
      },

      {
        fill: false,
        backgroundColor: "255,255,255",
        borderColor: dynamicColors,

        label:
          "Затраты на другие направления деятельности в сфере охраны окружающей среды",
        data: ["1506047", "131451", "1403464"],
      },
    ],
  });

  // const chartsData = {
  //   labels: currentStory.story.stats.years,
  //   datasets: [
  //     {
  //       label: currentStory.story.title + ` (${currentStory.story.units})`,
  //       data: currentStory.story.stats.values,
  //       yAxisID: "y-axis-2",
  //       backgroundColor: "#53b9e3",
  //     },
  //     currentStory.story.title == "Загрязняющие вещества в воздухе" && {
  //       fill: false,
  //       backgroundColor: "rgb(255, 99, 132)",
  //       borderColor: "rgba(255, 99, 132, 0.2)",
  //       yAxisID: "y-axis-1",
  //       label: "ПДК",
  //       data: [1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500],
  //     },
  //   ],
  // };

  return (
    <Modal isOpen={isOpen} o handler={handler}>
      <div className={"modal-content " + classes.mod}>
        <span style={{marginLeft: '20px'}}>Затраты</span>
        <Line
          options={{
            legend: {
              labels: {
                fontColor: "white"
              },
            },
            scales: {
              yAxes: [
                {
                  type: 'linear',
                  display: true,
                  position: 'left',
                  id: 'y-axis-1',
                  ticks: {
                    fontColor: "white"
                  }
                },
                {
                  type: 'linear',
                  display: true,
                  position: 'right',
                  id: 'y-axis-2',
                  gridLines: {
                    drawOnArea: false,
                  },
                  ticks: {
                    fontColor: "white"
                  }
                },
              ],
              xAxes: [{
                ticks: {
                  fontColor: "white"
                }
              }]
            },}}
          data={chartsData}
        />
        <div style={{marginLeft: '20px', marginTop: '40px' }}>Инвестиции</div>
        <Line
          options={{
            legend: {
              labels: {
                fontColor: "white"
              },
            },
            scales: {
              yAxes: [
                {
                  type: 'linear',
                  display: true,
                  position: 'left',
                  id: 'y-axis-1',
                  ticks: {
                    fontColor: "white"
                  }
                },
                {
                  type: 'linear',
                  display: true,
                  position: 'right',
                  id: 'y-axis-2',
                  gridLines: {
                    drawOnArea: false,
                  },
                  ticks: {
                    fontColor: "white"
                  }
                },
              ],
              xAxes: [{
                ticks: {
                  fontColor: "white"
                }
              }]
            },}}
          data={investmentsData}
        />
      </div>
    </Modal>
  );
}

export default ModalComponent;
