import React, { useState, useRef, useEffect } from "react";
import ReactMapGL, { FlyToInterpolator } from "react-map-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import { MAPBOX } from "constants.js";
import { makeStyles } from "@material-ui/core/styles";
import "mapbox-gl/dist/mapbox-gl.css";
import mapboxgl from "mapbox-gl";
import * as turf from '@turf/turf';
import { useSelector, useDispatch } from "react-redux";
import { fetchStoriesForId, fetchStoriesForIdMunic, hiddenStory, getCurrentStat } from "redux/actions";
import {diff} from "mapbox-gl/dist/style-spec/index.es";

const useStyles = makeStyles(() => ({
  mapWrapper: {
    height: "100%",
    width: "100%",
    position: "fixed",
    top: 0,
    left: 0,
  },
}));

// eslint-disable-next-line import/no-webpack-loader-syntax
mapboxgl.workerClass = require("worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker").default;

function DeckMap() {
  let _map = useRef(null);
  const classes = useStyles();
  const [isMapReady, setMapReady] = useState(false);
  const currentStory = useSelector((state) => state.scrollama.stories);
  const currentStep = useSelector((state) => state.scrollama.currentStory);
  const currentYear = useSelector((state) => state.app.currentYear);
  const currentStat = useSelector((state) => state.app.currentStat);
  const [viewport, setViewport] = useState({
    ...MAPBOX.VIEWPORT,
  });
  const parentRef = useRef(null);
  const dispatch = useDispatch();
  const isShown = useSelector((state) => state.scrollama.isShown);
  const isHidden = useSelector((state) => state.scrollama.isHidden);

  const handleClick = (event) => {
    const feature = event.features[0];
    if(isShown){
      if(!isHidden){
        dispatch(hiddenStory());
      }
    } else {
      if(feature.layer.id === "regions"){
        dispatch(fetchStoriesForId(feature.properties.id));
      }
      if(feature.layer.id === "municipalities"){
        dispatch(fetchStoriesForIdMunic(feature.properties.id));
      }
    }
  };

  useEffect(() => {
    if(currentStory && currentStory.extent){
      const bboxArray = turf.bbox(currentStory.extent);

      const bbox = [ [bboxArray[0],bboxArray[1]], [bboxArray[2], bboxArray[3]] ];

      const flyToProps = _map.current.getMap().cameraForBounds(bbox, {
        padding: {top: 10, bottom: 10, left: 10, right: 10}
      });

      setViewport({
        latitude: flyToProps.center.lat,
        longitude: flyToProps.center.lng,
        zoom: flyToProps.zoom,
        transitionDuration: 1000,
        pitch: 45,
        transitionInterpolator: new FlyToInterpolator(),
      });
    } else {
      setViewport(curViewport => (
        {
          ...curViewport,
          transitionDuration: 300,
          pitch: 0,
          transitionInterpolator: new FlyToInterpolator(),
        }
      ));
    }
  }, [currentStory, currentStep]);

  useEffect(() => {
    if(currentStory && currentStory.id && currentStep && currentStep.key){
      dispatch(getCurrentStat(currentYear, currentStep.key + currentStory.type));
    }
  }, [currentStory, currentStep, currentYear]);

  function pickHex(weight) {
    const color1 = [255, 0, 0];
    const color2 = [0, 255, 0];
    var w1 = weight;
    var w2 = 1 - w1;
    var rgb = [Math.round(color1[0] * w1 + color2[0] * w2),
      Math.round(color1[1] * w1 + color2[1] * w2),
      Math.round(color1[2] * w1 + color2[2] * w2)];
    return rgb;
  }

  useEffect(() => {
    if(currentStat){
      const key = currentStory.type === "reg" ? "reg_id" : "mun_id";
      const layerId = currentStory.type === "reg" ? "regions" : "municipalities";
      const differentLayerId = currentStory.type === "reg" ? "municipalities" : "regions";
      const maxHeight = currentStory.type === "reg" ? 300000 : 150000;

      const values = currentStat.regions.map((reg) => (reg.value && reg.value.length > 0 ? (+reg.value) : 0));
      const coef = maxHeight/Math.max(...values);

      let layerStyle = ["match",["get", "id"]];
      currentStat.regions.map((reg) => {
        layerStyle.push(reg[key], (reg.value && reg.value.length > 0 ? (+reg.value * coef) : 0));
      })
      layerStyle.push(0);

      let layerColor = ["match",["get", "id"]];
      currentStat.regions.map((reg) => {
        console.log(+reg.value, +reg.value * coef);
        const color = pickHex(((+reg.value * coef)/maxHeight));
        layerColor.push(reg[key], (reg.value && reg.value.length > 0 ? 'rgb(' + color.join(',') + ')' : "#0576f0"));
      })
      layerColor.push("#0576f0");
      console.log(layerColor);

      _map.current.getMap().setPaintProperty(layerId, 'fill-extrusion-height', layerStyle);
      _map.current.getMap().setPaintProperty(layerId, 'fill-extrusion-color', layerColor);

      _map.current.getMap().getLayer(layerId).maxzoom = undefined;
      _map.current.getMap().getLayer(layerId).minzoom = undefined;

      _map.current.getMap().setLayoutProperty(differentLayerId, 'visibility', 'none');
    } else {
      if(isMapReady){
        _map.current.getMap().setPaintProperty('regions', 'fill-extrusion-height', 0);
        _map.current.getMap().setPaintProperty('regions', 'fill-extrusion-color', "#0576f0");
        _map.current.getMap().setPaintProperty('municipalities', 'fill-extrusion-height', 0);
        _map.current.getMap().setPaintProperty('municipalities', 'fill-extrusion-color', "#0576f0");
        _map.current.getMap().getLayer('regions').maxzoom = 4;
        _map.current.getMap().getLayer('regions').minzoom = undefined;
        _map.current.getMap().getLayer('municipalities').maxzoom = undefined;
        _map.current.getMap().getLayer('municipalities').minzoom = 4;
        _map.current.getMap().setLayoutProperty('regions', 'visibility', 'visible');
        _map.current.getMap().setLayoutProperty('municipalities', 'visibility', 'visible');
      }
    }
  }, [currentStat])

  return (
    <div className={classes.mapWrapper}>
        <ReactMapGL
          ref={_map}
          {...viewport}
          onLoad={() => setMapReady(true)}
          width="100%"
          height="100%"
          onViewportChange={(vp) => setViewport(vp)}
          mapboxApiAccessToken={MAPBOX.API_TOKEN}
          mapStyle={MAPBOX.VIEWPORT.mapStyle}
          onClick={event => handleClick(event)}

        />
    </div>
  );
}

export default DeckMap;
