import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from "react-redux";
import Fab from '@material-ui/core/Fab';
import Close from '@material-ui/icons/Close';
import {hideStories} from '../../redux/actions'

const useStyles = makeStyles((theme) => ({
    root: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
    extendedIcon: {
      marginRight: theme.spacing(1),
    },
    showMenu: {
        position: 'fixed',
        zIndex: 9999999,
        cursor: 'pointer',
        top: '125px',
        right: '10px'
    }
  }));


export function CloseStorytellingBtn(props) {
    const classes = useStyles();


    return(
    props.isShown && <Fab className={classes.showMenu} onClick={props.hideStories} color="primary">
        <Close/>
    </Fab>
    )
}

const mapStateToProps = state => {
 return {
     isOpen: state.navigationDrawer.isOpen,
     isShown: state.scrollama.isShown
 }
}
export default connect(
    mapStateToProps,
    {hideStories})(CloseStorytellingBtn)
