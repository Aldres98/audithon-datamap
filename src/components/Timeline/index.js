import React, {useEffect, useState} from "react";
import { Grid } from '@material-ui/core';
import Nouislider from "nouislider-react";
import "nouislider/distribute/nouislider.css";
import wNumb from "wnumb";
import {useDispatch, useSelector} from "react-redux";
import {GET_STORIES} from "redux/actionTypes";

const Timeline = () => {
  const dispatch = useDispatch();
  const [currentYear, setYear] = useState(2016);

  useEffect(() => {
    dispatch({
      type: "SET_CURRENT_YEAR",
      payload: currentYear,
    });
    dispatch({
      type: "SET_CACHE_YEAR",
      payload: currentYear,
    });
  }, [currentYear]);

  return (
    <Grid className="timeline-wrap">
      <Nouislider
        tooltips={true}
        pips={{ mode: 'count', values: 2 }}
        ariaFormat={wNumb({
          decimals: 0
        })}
        format={wNumb({
          decimals: 0,
          suffix: ''
        })}
        range={{min: 2011, max: 2017}}
        start={[currentYear]}
        end={[]}
        onChange={(year) => setYear(year[0])}
      />
    </Grid>
  );
};

export default Timeline;
