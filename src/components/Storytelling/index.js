import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {ArrowUpward, ArrowDownward} from "@material-ui/icons"
import { fetchStoriesForId, fetchCosts, fetchInvestments } from "redux/actions";
import { useSelector, useDispatch } from "react-redux";
import "./styles.css";
import { Typography, IconButton } from "@material-ui/core";
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import scrollama from "scrollama";
import { Line } from "react-chartjs-2";
import { openMoneyModal } from 'redux/actions';
import arrowDownImg from '../Storytelling/images/down-arrow.png';

const useStyles = makeStyles((theme) => ({
  modal: {
    overflowY: "auto",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  }
}));

export const Storytelling = (props) => {
  const dispatch = useDispatch();
  const isHidden = useSelector((state) => state.scrollama.isHidden);
  const stories = useSelector((state) => state.scrollama.stories);
  const currentYear = useSelector((state) => state.app.currentYear);
  const cachedYear = useSelector((state) => state.app.cachedYear);
  const isShown = useSelector((state) => state.scrollama.isShown);
  const currentChartPoint = useSelector((state) => state.scrollama.currentChart);
  const currentStory = useSelector((state) => state.scrollama.currentStory);
  const [percentageDiffNextYear, setPercentageDiffNextYear] = useState(0);
  const [percentageDiffPrevYear, setPercentageDiffPrevYear] = useState(0);
  const [open, setOpen] = React.useState(true);
  const [scroller, setScroller] = useState(scrollama());

  const [dataChart, setDataChart] = useState({});

  useEffect(() => {
    currentStory.story && currentStory.story.title == "Загрязняющие вещества в воздухе" ?     setDataChart({
      labels: currentStory.story.stats.years,
      datasets: [
        {
          label: currentStory.story.title + ` (${currentStory.story.units})`,
          data: currentStory.story.stats.values,
          yAxisID: 'y-axis-2',
          backgroundColor: '#53b9e3',
        },
        currentStory.story.title == "Загрязняющие вещества в воздухе" && {
          fill: false,
          backgroundColor: 'rgb(255, 99, 132)',
          borderColor: 'rgba(255, 99, 132, 0.2)',
          yAxisID: 'y-axis-1',
          label: "ПДК",
          data: [1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500]
        }
      ],
    }) : currentStory.story &&
      setDataChart({
        labels: currentStory.story.stats.years,
        datasets: [
          {
            label: currentStory.story.title + ` (${currentStory.story.units})`,
            data: currentStory.story.stats.values,
            backgroundColor: '#53b9e3',
            yAxisID: 'y-axis-2'
          },
        ],
      });
  }, [currentStory]);

  useEffect(() => {
    if (open && scroller && stories.plots && stories.plots.length > 0) {
      setTimeout(() => {
        scroller
          .setup({
            step: ".step",
            offset: 0.5,
            progress: true,
          })
          .onStepEnter((response) => {
            response.element.classList.add("active");

            dispatch({
              type: "SET_CURRENT_STORY",
              payload: {
                story: Object.values(stories.plots[Object.keys(stories.plots)[response.index]])[0],
                key: Object.keys(stories.plots[Object.keys(stories.plots)[response.index]])[0],
                index: response.index,
                direction: response.direction,
              },
            });


          })
          .onStepExit((response) => {
            dispatch({
              type: "SET_CURRENT_YEAR",
              payload: cachedYear,
            });
            response.element.classList.remove("active");
          });
      }, 300);
    }
  }, [scroller, open, stories]);

  const relDiff = (a, b) => {
    return  100 * Math.abs( ( a - b ) / ( (a+b)/2 ) );
   }


  let nextYearDiff = (currentChartPoint.currentPointValue - currentChartPoint.nextPointValue);
  let previousYearDiff = (currentChartPoint.currentPointValue - currentChartPoint.previousPointValue);

  return (
    <div style={{ width: '100%' }}>
      {isShown && (
        <div id="storytellingTemplate" style={{ opacity: (isHidden ? 0 : 1), transition: '0.5s' }}>
          <div id="story">
            <div className="story-block" id="storyHeader" style={{ pointerEvents: (isHidden ? 'none' : 'all') }}>
              {stories.region &&
                <Typography variant="h6">{stories.region}</Typography>
              }
              <Typography variant="h4">{stories.name}</Typography>
              <div style={{ float: 'right', marginTop: '-17px' }}>
                <Typography variant="subtitle1">Данные на карте за {currentYear} г.</Typography>
              </div>
            </div>
            <div className="animated infinite bounce history-arrow"><p>Листай вниз</p><img src={arrowDownImg}  alt="" /></div>
            <div className={"lefty"} id="storytellingFeatures" style={{ pointerEvents: (isHidden ? 'none' : 'all') }}>
              {stories &&
                stories.plots.map((story, index) => (
                  <div
                    id={"storytelling-block-" + index}
                    className="step"
                    key={index}
                  >
                    <div className="story-block">
                      <div className={'money-button'}>
                        <IconButton onClick={() => dispatch(openMoneyModal())}>
                          <MonetizationOnIcon />
                        </IconButton>
                      </div>
                      <div className="story-block-inner">
                        <p>{story[Object.keys(story)[0]].title}</p>
                        <div className="container">
                          <Line
                            options={{
                              legend: {
                                labels: {
                                  fontColor: "white"
                                },
                              },
                              scales: {
                                yAxes: [
                                  {
                                    type: 'linear',
                                    display: true,
                                    position: 'left',
                                    id: 'y-axis-1',
                                    ticks: {
                                      fontColor: "white"
                                    }
                                  },
                                  {
                                    type: 'linear',
                                    display: true,
                                    position: 'right',
                                    id: 'y-axis-2',
                                    gridLines: {
                                      drawOnArea: false,
                                    },
                                    ticks: {
                                      fontColor: "white"
                                    }
                                  },
                                ],
                                xAxes: [{
                                  ticks: {
                                    fontColor: "white"
                                  }
                                }]
                              },
                              hover: {
                                mode: "point",
                                intersect: false,
                                onHover: function (e, item) {
                                  if (item.length && item[0]._yScale.id != "y-axis-1") {
                                    const data =
                                      item[0]._chart.config.data.datasets[0]
                                        .data[item[0]._index];
                                    const year =
                                      item[0]._chart.config.data.labels[item[0]._index];
                                    const index = item[0]._index
                                    setPercentageDiffNextYear(Math.trunc(100*relDiff(data,currentStory.story.stats.values[index+1])));
                                    setPercentageDiffPrevYear(Math.trunc(100*relDiff(data, currentStory.story.stats.values[index-1] || 0)));

                                    dispatch({
                                      type: "SET_CURRENT_YEAR",
                                      payload: year,
                                    });

                                    dispatch({
                                      type: "SET_HOVER_CHART_STATE",
                                      payload: {
                                        currentPointIndex: index,
                                        currentPointValue: data,
                                        nextPointValue: currentStory.story.stats.values[index+1],
                                        previousPointValue: currentStory.story.stats.values[index-1] || 0
                                      },
                                    });
                                  }
                                },
                              },
                            }}
                            data={dataChart}
                          />
                          <p>По сравнению с предыдущим годом: <span className={previousYearDiff < 0 ? "decrease" : "increase"}>{(previousYearDiff<0?"" : "+") + Math.trunc(previousYearDiff)}{previousYearDiff > 0 ? <ArrowUpward className="arrowIcon"/> : <ArrowDownward className="arrowIcon"/>} {`(${percentageDiffPrevYear}%)`}</span></p>
                          <p className>По сравнению с последующим годом: <span className={nextYearDiff < 0 ? "decrease" : "increase"}>{(nextYearDiff<0?"" : "+") + Math.trunc(nextYearDiff)}{nextYearDiff > 0 ? <ArrowUpward className="arrowIcon"/> : <ArrowDownward className="arrowIcon"/>} {`(${percentageDiffNextYear}%)`}</span></p>
                        </div>
                        <p> </p>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Storytelling;
