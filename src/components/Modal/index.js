import React, { useEffect } from 'react';
import {
  Dialog,
  Fade,
  Backdrop,
  Grid,
  IconButton,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import { useSelector, useDispatch } from "react-redux";
import { fetchCosts, fetchInvestments } from "redux/actions";

const useStyles = makeStyles((theme) => ({
  content: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalHeader: {
    display: 'flex',
    justifyContent: 'flex-end',
    position: 'sticky',
    top: 0,
    right: 0,
  },
  closeButton: {
    margin: theme.spacing(1),
    color: '#fff',
  },
  modal: {
    color: '#fff',
    zIndex: '999999999999999!important',
  }
}));

function ModalComponent(params) {
  const {
    isOpen = true,
    handler,
    children,
  } = params;
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const dispatch = useDispatch()

  const handleOpen = () => {
    setOpen(true);
    handler(true);
  };

  const handleClose = () => {
    setOpen(false);
    handler(false);
  };

  useEffect(() => {
    if (isOpen) {
      handleOpen();
    } else {
      handleClose();
    }
  }, [isOpen]);

  return (
    <Dialog
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={classes.modal}
      open={open}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={open}>
        <div>
          <Grid container className={classes.modalHeader}>
            <Grid item>
              <IconButton className={classes.closeButton} aria-label="закрыть" onClick={handleClose}>
                <CloseIcon />
              </IconButton>
            </Grid>
          </Grid>
          <Grid className={classes.content}>{children}</Grid>
        </div>
      </Fade>
    </Dialog>
  );
}

export default ModalComponent;
