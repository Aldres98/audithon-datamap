import {
  CLOSE_MENU,
  MAP_READY,
  OPEN_MENU,
  SET_CURRENT_STORY,
  TOGGLE_STORYTELLING,
  START_FETCHING_STORIES,
  GET_STORIES,
  HIDE_STORIES,
  SET_HOVER_CHART_STATE,
  GET_STAT,
  HIDDEN_STORIES,
  OPEN_MONEY_MODAL,
  START_FETCHING_COSTS,
  START_FETCHING_INVESTMENTS,
  GET_COSTS,
  GET_INVESTMENTS,
} from "./actionTypes";

const BASE_URL = "http://165.227.165.180:8880/api/";

export const mapReady = () => ({
  type: MAP_READY,
  payload: {},
});

export const openMenu = () => ({
  type: OPEN_MENU,
  payload: {},
});

export const closeMenu = () => ({
  type: CLOSE_MENU,
  payload: {},
});

export const setCurrentStory = (payload) => ({
  type: SET_CURRENT_STORY,
  payload: payload,
});

export const toggleStorytelling = () => ({
  type: TOGGLE_STORYTELLING,
  payload: {},
});

export const startFetchingStories = () => ({
  type: START_FETCHING_STORIES,
  payload: {},
});

export const recieveStories = (payload) => ({
  type: GET_STORIES,
  payload: payload,
});

export const recieveStat = (payload) => ({
  type: GET_STAT,
  payload: payload,
});

export const hideStories = () => ({
  type: HIDE_STORIES,
  payload: {}
})

export const hiddenStory = () => ({
  type: HIDDEN_STORIES,
  payload: {}
})

export const openMoneyModal = (status = true) => ({
  type: OPEN_MONEY_MODAL,
  payload: status
})

export const setHoverChartState = (payload) => ({
  type: SET_HOVER_CHART_STATE,
  payload: payload
})

export const startFetchingCosts = () => ({
  type: START_FETCHING_COSTS,
  payload: {}
});

export const startFetchingInvestments = () => ({
  type: START_FETCHING_INVESTMENTS,
  payload: {}
})

export const recieveCosts = (payload) => ({
  type: GET_COSTS,
  payload: payload
})

export const recieveInvestments = (payload) => ({
  type: GET_INVESTMENTS,
  payload: payload
})


export const fetchStoriesForId = (id, url = "regions/") => {
  return (dispatch) => {
    dispatch(startFetchingStories());
    fetch(BASE_URL + url + id + '/')
      .then((response) => response.json())
      .then((json) => {
        dispatch(recieveStories({ ...json, type: 'reg' }));
        dispatch(toggleStorytelling);
      });
  };
};

export const fetchStoriesForIdMunic = (id, url = "munic/") => {
  return (dispatch) => {
    dispatch(startFetchingStories());
    fetch(BASE_URL + url + id + '/')
      .then((response) => response.json())
      .then((json) => {
        dispatch(recieveStories({ ...json, type: 'mun' }));
        dispatch(toggleStorytelling);
      });
  };
};

export const fetchCosts = (url = "arcticcosts/1/") => {
  return (dispatch) => {
    dispatch(startFetchingCosts());
    fetch(BASE_URL + url)
      .then((response) => response.json())
      .then((json) => {
        dispatch(recieveCosts(json));
      });
  };
}

export const fetchInvestments = (url = "arcticinvestments/1/") => {
  return (dispatch) => {
    dispatch(startFetchingInvestments());
    fetch(BASE_URL + url)
      .then((response) => response.json())
      .then((json) => {
        dispatch(recieveInvestments(json));
      });
  };
}


export const getCurrentStat = (year, url) => {
  return (dispatch) => {
    fetch(BASE_URL + url + '/?' + "year=" + year)
      .then((response) => response.json())
      .then((json) => {
        dispatch(recieveStat(json));
      });
  };
};

export const getCurrentStatByReg = (id, url, type) => {
  return (dispatch) => {
    fetch(BASE_URL + url + '/?' + (type === 'reg' ? "region=" : "munic=") + id)
      .then((response) => response.json())
      .then((json) => {
        dispatch(recieveStat(json));
      });
  };
};
