import { combineReducers } from 'redux';

import app from './app';
import navigationDrawer from './navigationDrawer'
import scrollama from './scrollama'

export default combineReducers({
  app,
  navigationDrawer,
  scrollama
});
