import { data } from 'jquery';
import { startFetchingCosts } from 'redux/actions';
import {
  SET_CURRENT_STORY,
  TOGGLE_STORYTELLING,
  START_FETCHING_STORIES,
  GET_STORIES,
  HIDE_STORIES,
  SET_HOVER_CHART_STATE,
  HIDDEN_STORIES,
  START_FETCHING_COSTS,
  GET_COSTS,
  START_FETCHING_INVESTMENTS,
  GET_INVESTMENTS
} from '../actionTypes'

const initialState = {
  isShown: false,
  isLoading: false,
  isFinished: false,
  isHidden: false,
  stories: [],
  currentStory: {},
  currentChart: {},
  costs: {
    isLoading: false,
    data: {}
  },
  investments: {
    isLoading: false,
    data: {}
  }
};

export default function (state = initialState, action) {
  switch (action.type) {
    case HIDDEN_STORIES: {
      return {
        ...state,
        isHidden: !state.isHidden,
      };
    }
    case SET_CURRENT_STORY: {
      return {
        ...state,
        currentStory: action.payload,
      };
    }
    case TOGGLE_STORYTELLING: {
        return {
            ...state,
            isShown: !this.isShown
        }
    }
    case START_FETCHING_STORIES: {
        return {
            ...state,
            isLoading: true,
            isFinished: false
        }
    }

    case GET_STORIES: {
        return {
            ...state,
            isLoading: false,
            isFinished: true,
            isShown: true,
            stories: action.payload
        }
    }

    case START_FETCHING_COSTS: {
      return {
        ...state,
        costs: {
          isLoading: true,
        }
      }
    }

    case GET_COSTS: {
      return {
        ...state,
        costs: {
          isLoading: false,
          data: action.payload
        }
      }
    }

    case START_FETCHING_INVESTMENTS: {
      return {
        ...state,
        investments: {
          isLoading: true,
        }
      }
    }

    case GET_INVESTMENTS: {
      return {
        ...state,
        investments: {
          isLoading: false,
          data: action.payload
        }
      }
    }


    case HIDE_STORIES: {
        return {
            ...state,
            isFinished: false,
            isShown: false,
            stories: [],
            currentStory: {},
        }
    }

    case SET_HOVER_CHART_STATE: {
      return {
        ...state,
        currentChart: {
          currentPointIndex: action.payload.currentPointIndex,
          currentPointValue: action.payload.currentPointValue,
          nextPointValue: action.payload.nextPointValue,
          previousPointValue: action.payload.previousPointValue

        }
      }
    }

    default:
      return state;
  }
}
