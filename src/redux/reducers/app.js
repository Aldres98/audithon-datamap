import {
  GET_STAT,
  MAP_READY,
  HIDE_STORIES, OPEN_MONEY_MODAL,
} from '../actionTypes';

const initialState = {
  isMapReady: false,
  currentYear: 2016,
  cachedYear: 2016,
  currentStat: null,
  isMoneyModalOpen: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case MAP_READY: {
      return {
        ...state,
        isMapReady: true,
      };
    }
    case GET_STAT: {
      return {
        ...state,
        currentStat: action.payload,
      };
    }
    case "SET_CURRENT_YEAR": {
      return {
        ...state,
        currentYear: +action.payload,
      };
    }
    case "SET_CACHE_YEAR": {
      return {
        ...state,
        cachedYear: +action.payload,
      };
    }
    case HIDE_STORIES: {
      return {
        ...state,
        currentStat: null,
      };
    }
    case OPEN_MONEY_MODAL: {
      return {
        ...state,
        isMoneyModalOpen: action.payload,
      };
    }
    default:
      return state;
  }
}
