import {CLOSE_MENU, OPEN_MENU} from '../actionTypes'

const initialState = {
  isOpen: false,
  selectedItems: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case OPEN_MENU: {
      return {
        ...state,
        isOpen: true,
      };
    }
    case CLOSE_MENU: {
        return {
            ...state,
            isOpen: false
        }
    }
    default:
      return state;
  }
}
